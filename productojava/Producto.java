public class Producto {


private int codigoDelProducto;
private String nombre;
private double precioDeCosto;
private double porcentajeDeGanancia;
private double ivaDelProducto;
private double precioDeVenta;


public void setCodigoDelProducto (int vcodigoDelProducto) {

this.codigoDelProducto = vcodigoDelProducto;

}

public int getCodigoDelProducto() {

    return this.codigoDelProducto;

}



public void setNombre (String vnombre){

this.nombre = vnombre;

}

public String getNombre(){

return this.nombre;

}





public void setPrecioDeCosto (double vprecioDeCosto){

this.precioDeCosto= vprecioDeCosto;

}

public double getPrecioDeCosto (){

return this.precioDeCosto;

}




public void setPorcentajeDeGanancia (double vporcentajeDeGanancia){

this.porcentajeDeGanancia = vporcentajeDeGanancia;

}

public double getPorcentajeDeGanancia() {

return this.porcentajeDeGanancia;

}




public void setIvaDelProducto (float vivaDelProducto){

this.ivaDelProducto = vivaDelProducto;

}

public double getIvaDelProducto() {

return this.ivaDelProducto;

}




public void setPrecioDeVenta  (double vprecioDeVenta){

this.precioDeVenta = vprecioDeVenta;

}

public double getPrecioDeVenta() {

return this.precioDeVenta;

}


@Override

public String toString(){

    return "El codigo del producto es: " + codigoDelProducto + " y el producto se llama: " + nombre + ".Su precio de Costo es: " + precioDeCosto + " y su precio de venta al publico es el siguiente: " + precioDeVenta;


    }




    
}