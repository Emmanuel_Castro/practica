

public class Ejecutable {
    


    public static void main (String[] args) {

        Libro libro1 = new Libro();
        Libro libro2 = new Libro();
    

        libro1.setIsbn(234465456);
        libro1.setAutor("Emmanuel Castro");
        libro1.setCantPaginas(250);

        libro2.setIsbn(21231321);
        libro2.setAutor("Gabrielo Cardozo");
        libro2.setCantPaginas(300);

        
 MostrarInformacion(libro1,libro2);

    
   

    }



    static void MostrarInformacion(Libro libro1, Libro libro2) {
        
        System.out.println ("El libro con ISBN " + libro1.getIsbn() + " creado por el autor " + libro1.getAutor() + " tiene " + libro1.getCantPaginas() + " paginas. ");
        System.out.println ("El libro con ISBN " + libro2.getIsbn() + " creado por el autor " + libro2.getAutor() + " tiene " + libro2.getCantPaginas() + " paginas. ");
   
   
       }
    



}
